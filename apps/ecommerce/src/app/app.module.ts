import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { UiMaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { BodyComponent } from './views/body/body.component';

@NgModule({
  imports: [BrowserModule, BrowserAnimationsModule, HttpClientModule, UiMaterialModule, AppRoutingModule],
  declarations: [AppComponent, BodyComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
