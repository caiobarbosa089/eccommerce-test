import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { BodyComponent } from './views/body/body.component';

const routes: Routes = [
  { path: '', redirectTo: 'settings', pathMatch: 'full' },
  {
    path: '',
    component: BodyComponent,
    children: [
      {
        path: 'settings',
        loadChildren: () => import('./views/settings/settings.module').then((m) => m.SettingsModule),
      },
    ],
  },
  {
    path: 'auth',
    loadChildren: () => import('./views/auth/auth.module').then((m) => m.AuthModule),
  },
  { path: '**', redirectTo: 'settings', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
