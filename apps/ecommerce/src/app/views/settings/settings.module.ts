import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiMaterialModule } from '../../material.module';
import { SettingsHomeComponent } from './settings-home/settings-home.component';
import { AddressConfirmationComponent } from './address-confirmation/address-confirmation.component';
import { NewAddressComponent } from './address-confirmation/new-address/new-address.component';
import { ExistingAddressesComponent } from './address-confirmation/existing-addresses/existing-addresses.component';

@NgModule({
  imports: [CommonModule, SettingsRoutingModule, FormsModule, ReactiveFormsModule, UiMaterialModule],
  declarations: [
    SettingsHomeComponent,
    AddressConfirmationComponent,
    NewAddressComponent,
    ExistingAddressesComponent,
  ],
})
export class SettingsModule {}
