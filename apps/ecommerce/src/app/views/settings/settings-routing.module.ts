import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddressConfirmationComponent } from './address-confirmation/address-confirmation.component';
import { SettingsHomeComponent } from './settings-home/settings-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'address-confirmation', pathMatch: 'full' },
  {
    path: '',
    component: SettingsHomeComponent,
    children: [
      {
        path: 'address-confirmation',
        component: AddressConfirmationComponent,
      },
    ],
  },
  { path: '**', redirectTo: 'address-confirmation', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsRoutingModule {}
