import { Component, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';

@Component({
  selector: 'app-existing-addresses',
  templateUrl: './existing-addresses.component.html',
  styleUrls: ['./existing-addresses.component.scss'],
})
export class ExistingAddressesComponent {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  addresses = [
    {
      title: 'Address 1',
      street: '123 Addrees Street',
      zipCode: '74827364',
      city: 'City ST',
      phone: '+1 234 9874689743',
    },
    {
      title: 'Address 2',
      street: '123 Addrees Street',
      zipCode: '74827364',
      city: 'City ST',
      phone: '+1 234 9874689743',
    },
    {
      title: 'Address 3',
      street: '123 Addrees Street',
      zipCode: '74827364',
      city: 'City ST',
      phone: '+1 234 9874689743',
    },
    {
      title: 'Address 4',
      street: '123 Addrees Street',
      zipCode: '74827364',
      city: 'City ST',
      phone: '+1 234 9874689743',
    },
  ];
}
