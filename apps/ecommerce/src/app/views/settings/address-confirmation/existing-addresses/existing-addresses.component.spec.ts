import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistingAddressesComponent } from './existing-addresses.component';

describe('ExistingAddressesComponent', () => {
  let component: ExistingAddressesComponent;
  let fixture: ComponentFixture<ExistingAddressesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExistingAddressesComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ExistingAddressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
