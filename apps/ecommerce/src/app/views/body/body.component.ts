import { Component } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss'],
})
export class BodyComponent {
  menuList = [
    { key: 'dashboard', label: 'Dashboard', link: '/facilitator/dashboard' },
    { key: 'manageEvents', label: 'Manage Events', link: '/facilitator/manage-events' },
    { key: 'projection', label: 'Projection', link: '/facilitator/projection' },
    { key: 'manageUser', label: 'Manage User', link: '/facilitator/manage-user' },
  ];
  selected = this.menuList[0];
}
