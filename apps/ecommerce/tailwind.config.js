module.exports = {
  content: ['./apps/**/src/**/*.{html,ts}'],
  theme: {
    extend: {},
  },
  plugins: [require('postcss-import'), require('tailwindcss'), require('autoprefixer')],
};
